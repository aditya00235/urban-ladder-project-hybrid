package uistore;

import org.openqa.selenium.By;

public class PopUpPageUI 
{
	public static By popUpClose = By.xpath("//a[@data-gaaction=\"popup.auth.close\"]");
	public static By wholePopUpWindow=By.xpath("//div[@class='reveal-modal-bg']");
}
